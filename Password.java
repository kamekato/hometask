package com.company;

public class Password {
    public boolean checkPassword(String passwordStr) {
        if (passwordStr.length() < 9) {
            return false;
        }
        if (!checkStr(passwordStr)) {
            return false;
        }
        return true;

    }
    public boolean checkStr(String passwordStr) {
        char pass;
        boolean capital = false;
        boolean lower = false;
        boolean number = false;
        for (int i = 0; i < passwordStr.length(); i++) {
            pass = passwordStr.charAt(i);
            if (Character.isDigit(pass)) {
                number = true;
            } else if (Character.isUpperCase(pass)) {
                capital = true;
            } else if (Character.isLowerCase(pass)) {
                lower = true;
            }
            if (number && capital && lower)
                return true;
        }
        return false;
    }
}
