package com.company;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        MyApplication application = new MyApplication();
        System.out.println("An application is about to start..");
        System.out.println("Welcome to my application!");
        application.start();
    }
}
