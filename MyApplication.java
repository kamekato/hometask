package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication extends User{
    // users - a list of users
    private ArrayList<User> Users = new ArrayList<>();
    private Scanner sc = new Scanner(System.in);
    private User signedUser;

    private void addUser(User user) {
        Users.add(user);
    }



    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else {
                userProfile();
            }
        }
    }

    private void userProfile() {
        signedUser = null;
        System.out.println("Hello! How Are You?");

    }

    private void logOff() {
        signedUser = null;
    }

    private void authentication() {
        // sign in
        // sign up
        Scanner sc = new Scanner(System.in);
        System.out.println("You are not Signed in");
        System.out.println("1. Sign In");
        System.out.println("2. Sign Up");
        System.out.println("3. Menu");
        int choice = sc.nextInt();
        if(choice == 1){
            signIn();
        }
        else if(choice == 2){
            signUp();
        }
        else{
            menu();
        }
    }

    private void signIn() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, your username: ");
        String username;
        username = sc.nextLine();
        while(findUsername(username)){
            System.out.println("This username does not exist");
            System.out.println("Please, try again");
            username = sc.nextLine();
        }
        System.out.println("Please, your password: ");
        String pass;
        pass = sc.nextLine();
        Password password = new Password();
        if(!password.checkPassword(pass)){
            System.out.println("Incorrect password. Please, Try again");
        }
        userProfile();
    }

    private boolean findUsername(String username) {
        for(User user : Users)
        {
            if(user.getUsername().equals(username))
            {
                return true;
            }
        }
        return false;
    }

    private void signUp() {
        Scanner sc = new Scanner(System.in);
        String name, surname, username, password;
        System.out.println("Please, Your name: ");
        name = sc.nextLine();
        System.out.println("Please, Your surname: ");
        surname = sc.nextLine();
        System.out.println("Please, Your Username");
        username = sc.nextLine();
        if(findUsername(username)){
            System.out.println("This login already exists. Please, choose another username: ");
            username = sc.nextLine();
        }
        System.out.println("Please, Your password: ");
        password = sc.nextLine();
        User n = new User(name, surname, username, password);
        signedUser = n;
        Users.add(n);
        signIn();
    }

    public void start() throws FileNotFoundException {
        File file = new File("D:\\db.txt");
        Scanner fileScanner = new Scanner(file);
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

        // save the userlist to db.txt
    }

    private void saveUserList() throws IOException {
        String content = "";
        for(User user : Users){
            content += user.getId() + " " + user.getName() + " " + user.getSurname() + " " + user.getUsername() + " " + user.getPassword() + "\n";
        }
        Files.write(Paths.get("D:\\db.txt"), content.getBytes());
    }
}

