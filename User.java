package com.company;


public class User extends Password{
    private static int id_gen = 1, mx = 1;
    private String name,surname,password,username;
    private int id;

    public User() {
        id = id_gen++;
        mx++;
    }

    public User (String Name, String Surname, String Username, String Password) {
        this();
        setName(Name);
        setSurname(Surname);
        setUsername(Username);
        setPassword(Password);
    }
    public void setName(String a) { name = a; }
    public void setSurname(String a) { surname = a; }
    public void setUsername(String a) { username = a; }
    public void setPassword(String a) { password = a; }

    public int getId() { return id; }
    public String getName() { return name; }
    public String getSurname() { return surname; }
    public String getUsername() { return username; }
    public String getPassword() { return password; }

}